const express = require("express");
const fs = require('fs');
var cors = require('cors');

const app = express();
app.use(cors())
const port = 3000;

app.get('/', (req, res) => {
  let rawdata = fs.readFileSync('data.json');
  let products = JSON.parse(rawdata);
  res.json(products)
});

app.listen(port, () => console.log(`API Listening on port ${port}!`));
